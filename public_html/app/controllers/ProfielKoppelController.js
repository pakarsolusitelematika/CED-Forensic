angular.module('workshop').controller('ProfielKoppelController', function ($scope, bedrijfService, $routeParams) {
    var construct = function () {
        $scope.userid = $routeParams.id;

        bedrijfService.getFilterdBedrijven($routeParams.id,
            function (bedrijven) {
                $scope.bedrijven = bedrijven;
                $scope.loading = false;
            });
    };

    construct();

    $scope.koppelBedrijf = function (bedrijfid) {
        bedrijfService.koppel($routeParams.id, bedrijfid, function () {
            $scope.gotoProfielView($routeParams.id);
        });
    };
});
