angular.module('workshop').controller('AppController', function ($scope, $location, authenticationService) {
    $scope.isLocation = function (location) {
        return $location.path() === location;
    };

    $scope.gotoHome = function () {
        $location.path('/');
    };
    
    $scope.goBack = function() {
        window.history.back();
    };

    $scope.gotoLogin = function () {
        $location.path('/login');
    };

    $scope.gotoRegistration = function () {
        $location.path('/register');
    };

    $scope.gotoProfiel = function () {
        $location.path('/profiel');
    };

    $scope.gotoClientAdd = function () {
        $location.path('/clientadd');
    };

    $scope.gotoDossier = function () {
        $location.path('/dossier');
    };

    $scope.gotoDossierAdd = function () {
        $location.path('/dossieradd');
    };
    
    $scope.gotoDossierView = function (id) {
        $location.path('/dossierview/' + id);
    };

    $scope.gotoDossierEdit = function (id) {
        $location.path('/dossieredit/' + id);
    };

    $scope.gotoDossierArchive = function () {
        $location.path('/dossierarchive');
    };

    $scope.gotoProfielAdd = function () {
        $location.path('/profieladd');
    };

    $scope.gotoProfielEdit = function (id) {
        $location.path('/profieledit/' + id);
    };

    $scope.gotoProfielView = function (id) {
        $location.path('/profiel/' + id);
    };
    
    $scope.gotoZoeken = function ()
    {
        $location.path('/zoeken');
    };

    $scope.logout = function () {
        authenticationService.deleteAuthentication();
        $scope.gotoHome();
    };
    
    $scope.gotoprototypeView = function () {
        $location.path('/prototypeview');
    };

    $scope.gotoManagementInfo = function () {
        $location.path('/managementinfo');
    };


    
    
});

angular.module('workshop').filter('filterAll', function ($filter) {
    return function (inputArray, searchText) {
        var wordArray = searchText ? searchText.toLowerCase().split(/\s+/) : [];
        var wordCount = wordArray.length;
        for (var i = 0; i < wordCount; i++) {
            inputArray = $filter('filter')(inputArray, wordArray[i]);
        }
        return inputArray;
    }
});

angular.module('workshop').filter('isGroup', function () {
    return function (values, groupId) {
        if (!values) {
            return;
        } else if (!groupId) {
            return values.filter(function (value) {
                return value.active === 1;
            });
        } else if (groupId === 4) {
            return values.filter(function (value) {
                return value.active === 0;
            });
        } else {
            return values.filter(function (value) {
                return value.rol === groupId && value.active === 1;
            });
        }
    };
});

angular.module('workshop').filter('join', function () {
    return function (array, separator, prop) {
        if (!Array.isArray(array)) {
            return array;
        }

        return (!!prop ? array.map(function (item) {
            return item[prop];
        }) : array).join(separator);
    };
});
