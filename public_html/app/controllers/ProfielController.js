angular.module('workshop').controller('ProfielController', function ($scope, profielService, alertify) {

    $scope.users = undefined;
    console.log('random');

    $scope.construct = function () {
        profielService.getAll().then(function (response) {
            $scope.users = response.data;
            $scope.loading = false;
        });
    };

    $scope.deleteUser = function (userid) {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze gebruiker wilt verwijderen?", function () {
                profielService.delete(userid, function () {
                    $scope.construct();
                });
            }, function () {
            });
    };

    $scope.undeleteUser = function (userid) {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze gebruiker wilt terug zetten?", function () {
                profielService.undelete(userid, function () {
                    $scope.construct();
                });
            }, function () {
            });
    };
    
    $scope.getAll = function () {
        profielService.getAll().then(function(response) {
            $scope.users = response.data;
        });
    };

    $scope.selectedGroup = '';
    $scope.setGroup = function (group) {
        if ($scope.selectedGroup === group) {
            $scope.selectedGroup = '';
        } else {
            $scope.selectedGroup = group;
        }
    };

    $scope.construct();
});
