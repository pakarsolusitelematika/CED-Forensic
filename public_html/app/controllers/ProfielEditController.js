angular.module('workshop').controller('ProfielEditController', function ($scope, profielService, $routeParams, alertify) {
    var construct = function () {
        profielService.getUser(function (user) {
            $scope.names = ["Admin", "Beheerder", "Gebruiker"];
            $scope.user = user;
            $scope.voornaam = user.voornaam;
            $scope.tussenvoegsel = user.tussenvoegsel;
            $scope.achternaam = user.achternaam;
            $scope.telefoonnummer = user.telefoonnummer;
            $scope.email = user.email;
            $scope.selectedRol = $scope.names[user.rol - 1];
        }, $routeParams.id);
    };
 
    construct();

    $scope.edit = function () {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze gebruiker wilt wijzigen?", function () {
                profielService.update($scope.user.id, $scope.selectedRol,
                    $scope.telnr, $scope.voornaam, $scope.achternaam, $scope.tussenvoegsel,
                    $scope.email, $scope.wachtwoord, function () {
                        $scope.gotoProfielView($scope.user.id);
                    });
            }, function () {
            });
    };
});
