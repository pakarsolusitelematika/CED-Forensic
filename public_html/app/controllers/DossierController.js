angular.module('workshop').controller('DossierController', function ($scope, dossierService, alertify) {
    var construct = function () {
        dossierService.getAll(function (files) {


            $scope.files = files;
            $scope.loading = false;

        });
    };
    construct();
});
