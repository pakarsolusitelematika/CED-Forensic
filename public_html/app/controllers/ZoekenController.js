angular.module('workshop').controller('ZoekenController', function ($scope, profielService, bedrijfService) {
    var countCount = 0;

    var checkResponseCount = function () {
        countCount++;

        if (countCount === 2) {
            $scope.loading = false;
        }
    };

    var construct = function () {
        profielService.getAllZoeken(function (users) {
            $scope.users = users;
            checkResponseCount();
        });

        bedrijfService.getBedrijvenZoeken(function (bedrijven) {
            $scope.bedrijven = bedrijven;
            checkResponseCount();
        });
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
    };

    construct();
});
