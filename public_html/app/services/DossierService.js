angular.module('workshop').service('dossierService', function ($http) {
    var self = this;
    self.getStatus = function (onReceived) {
        var uri = 'http://localhost:8080/api/files/states';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };
    self.getExperts = function (onReceived) {
        var uri = 'http://localhost:8080/api/users';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    }
    self.getClients = function (onReceived) {
        var uri = 'http://localhost:8080/api/files/clients';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    }
    self.getAll = function (onReceived) {
        var uri = 'http://localhost:8080/api/files';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };
    self.getDossier = function (onReceived, id) {
        var uri = 'http://localhost:8080/api/files/' + id;
        console.log("DossierService getDossier");
        $http.get(uri).then(function (response) {
                onReceived(response.data);
                
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };
    self.addMobility = function(onReceived, mobility_postcode, mobility_huisnr,
                                mobility_naam_verzekerde, mobility_kenteken) {
        var uri = 'http://localhost:8080/api/files/mobility';
        var data =
            {
                id : 0,
                postcode:mobility_postcode,
                huisnr:mobility_huisnr,
                naam_verzekerde:mobility_naam_verzekerde,
                mobility_kenteken:mobility_kenteken,
                mobility_automaat:0
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });
    };
    self.addVitality = function(onReceived, vitality_slachtoffernaam,
                                vitality_beschrijving, vitality_postcode, 
                                vitality_huisnr) {
        var uri = 'http://localhost:8080/api/files/vitality';
        var data =
            {
                id : 0,
                vitality_slachtoffernaam:vitality_slachtoffernaam,
                vitality_beschrijving:vitality_beschrijving,
                postcode:vitality_postcode,
                huisnr:vitality_huisnr
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });
    };
    self.archive = function(id, onReceived){
        var uri = 'http://localhost:8080/api/files/' + id;

        $http.delete(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };
    self.recover = function(id, onReceived){
        var uri = 'http://localhost:8080/api/files/' + id;

        $http.post(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };

    self.setStatus = function(id, statusId, onReceived){

        console.log("self.setStatus naar status: " + statusId + " in DossierService op DossierID: " + id);

        var uri = 'http://localhost:8080/api/files/' + id;

        $http.post(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };
    self.addProperty = function(onReceived, property_plaats, property_adres, 
                                property_huisnummer, property_toevoeging, 
                                property_type, property_postcode, 
                                property_naam_verzekerde) {
        var uri = 'http://localhost:8080/api/files/property';
        var data =
            {
                id : 0,
                property_plaats : property_plaats,
                property_adres : property_adres,
                property_huisnr : property_huisnummer,
                property_toevoeging : property_toevoeging,
                property_type : property_type,
                postcode : property_postcode,
                naam_verzekerde : property_naam_verzekerde
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });
    };

    self.createClient = function (onReceived, Naam, Telefoonnummer) {
        var uri = 'http://localhost:8080/api/files/clientadd';
        var data =
            {
                naam : Naam,
                telefoonnummer : Telefoonnummer
            };
        console.log("DossierService createClient");
        console.log(data);



        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });
    };
    
    self.create = function (onReceived, Status, Expert, Opdrachtgever, 
    Dossiernummer, Schade_nummer, Opdrachtdatum, Polisnummer, 
    Kwalificatie, Datum_Nieuw_Reg, Stratiedatum, Afdeling) {
        var uri = 'http://localhost:8080/api/files';
        var data =
            {
                status : Status,
                expert: Expert,
                opdrachtgever: Opdrachtgever,
                nummer : Dossiernummer,
                schadenummer : Schade_nummer,
                opdrachtdatum : Opdrachtdatum,
                polisnummer : Polisnummer,
                kwalificatie : Kwalificatie,
                nieuw_registratiedatum : Datum_Nieuw_Reg,
                stratiedatum : Stratiedatum,
                afdeling: Afdeling
                
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });
    };
    
});