
angular.module('workshop').config(function($routeProvider)
{
    $routeProvider.when('/', { templateUrl: 'assets/partials/login.html', controller: 'LoginController' });
    $routeProvider.when('/profiel', { templateUrl: 'assets/partials/profiel.html', controller: 'ProfielController' });
    $routeProvider.when('/profiel/:id', { templateUrl: 'assets/partials/profielview.html', controller: 'ViewProfielController' });
    $routeProvider.when('/profieledit/:id', { templateUrl: 'assets/partials/profieledit.html', controller: 'ProfielEditController' });
    $routeProvider.when('/profieladd', { templateUrl: 'assets/partials/profieladd.html', controller: 'ProfielAddController' });
    $routeProvider.when('/profielkoppelen/:id', { templateUrl: 'assets/partials/profielkoppelen.html', controller: 'ProfielKoppelController' });
    $routeProvider.when('/zoeken', { templateUrl: 'assets/partials/zoeken.html', controller: 'ZoekenController' });
    
    
    $routeProvider.when('/prototypeview', { templateUrl: 'assets/partials/prototypeview.html' });


    $routeProvider.when('/clientadd', { templateUrl: 'assets/partials/clientadd.html', controller: 'DossierAddController' });

    $routeProvider.when('/dossier', { templateUrl: 'assets/partials/dossier.html', controller: 'DossierController' });
    $routeProvider.when('/dossieradd', { templateUrl: 'assets/partials/dossieradd.html', controller: 'DossierAddController' });
    $routeProvider.when('/dossierview/:id', { templateUrl: 'assets/partials/dossierview.html', controller: 'ViewDossierController' });
    $routeProvider.when('/dossieredit/:id', { templateUrl: 'assets/partials/dossieredit.html', controller: 'DossierEditController' });
    $routeProvider.when('/dossierarchive', { templateUrl: 'assets/partials/dossierarchive.html', controller: 'DossierController' });

    $routeProvider.when('/managementinfo', { templateUrl: 'assets/partials/managementinfo.html', controller: 'ManagementInfoController' });


    
    // Voeg hier meer routes toe
    
    $routeProvider.otherwise({ redirectTo: '/' });
});
