
(function()
{
    // JQuery
    addScript('assets/libraries/jquery.min.js');

    // AngularJs
    addScript('assets/libraries/angular.min.js');
    addScript('assets/libraries/angular-route.min.js');
    addScript('assets/libraries/ngAlertify.js');
    

    // Bootstrap
    addScript('assets/libraries/ui-bootstrap.min.js');
    addScript('assets/libraries/bootstrap.min.js');

    // Other
    addScript('assets/libraries/base64.min.js');
    addScript('assets/libraries/dirPagination.js');

    addScript('https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js');
    addScript('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js');
    addScript('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/nl.js');
    addScript('assets/libraries/clndr.min.js');
    addScript('assets/libraries/datePicker/js/datepicker.js');
    
    addScript('assets/libraries/taggle.js');
    addScript('assets/libraries/taggle-ie8.js');
    addScript('assets/libraries/taggle-ie9.js');


    function addScript(url)
    {
        document.write('<script type="text/javascript" src="' + url + '"></script>');
    }
})();
